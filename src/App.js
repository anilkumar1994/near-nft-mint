import { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import MintForm from './MintForm';
import 'bootstrap/dist/css/bootstrap.min.css';
import Logo from './near_icon.svg';
import Big from 'big.js';
import { Row, Col } from 'reactstrap';

const SUGGESTED_DONATION = '0';
const BOATLOAD_OF_GAS = Big(3).times(10 ** 13).toFixed();

function App({ contract, currentUser, nearConfig, wallet }) {
  const [nfts, setNfts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isNew, setIsNew] = useState(false);

  useEffect(() => {
    if (currentUser)
      contract.nft_tokens_for_owner({ account_id: currentUser.accountId }).then(setNfts);
  }, []);

  useEffect(() => {
    if (window.location.search.includes('transactionHashes')) {
      window.location.replace(window.location.origin);
    }

  }, [window.location.search])

  const onSubmit = (e, data) => {
    e.preventDefault();
    setLoading(true);
    contract.nft_mint(
      {
        "token_id": `${new Date().getTime()}`,
        "receiver_id": currentUser.accountId,
        "token_metadata": data
      },
      BOATLOAD_OF_GAS,
      Big('0.5').times(10 ** 24).toFixed(),
    ).then((res) => {

    });
  };

  const signIn = () => {
    wallet.requestSignIn(
      nearConfig.contractName,
      'NEAR Guest Book'
    );
  };

  const signOut = () => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
  };

  return (
    <div className="App">
      <header >
        <img src={Logo} />
        <span>Mint NFT with NEAR</span>
        {currentUser
          ? <button onClick={signOut}>Log out</button>
          : <button onClick={signIn}>Log in</button>
        }
      </header>
      {currentUser ? (<>
        {!isNew && <button onClick={() => setIsNew(true)} className="new">Mint New NFT</button>}
        {isNew && <MintForm onSubmit={onSubmit} setIsNew={setIsNew} />}
        {nfts.length > 0 ?
          <div className="nfts-container">
            <h4>Minted NFTs</h4>
            <Row className="nfts">
              {nfts.map(nft => (
                <Col className="nft">
                  <img src={nft.metadata.media} />
                  <span>{nft.metadata.title}</span>
                </Col>
              ))}
            </Row>
          </div> :
          <span className="no-nfts">No NFTs minted yet!</span>
        }
      </>)
        :
        <div>Login with NEAR to continue... </div>
      }
    </div>
  );
}

export default App;
