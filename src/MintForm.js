import { useState } from "react";
import { FormGroup, Form, Input, Label, FormText, Button, Row, Col } from 'reactstrap';


const MintForm = ({ onSubmit, setIsNew }) => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [media, setMedia] = useState("");
    const [copies, setCopies] = useState("");
    return (
        <Row md="12" className="w-50">
            <Form onSubmit={(e) => onSubmit(e, { title, description, media, copies })}>
                <FormGroup>
                    <Label for="title">
                        Title
                    </Label>
                    <Input
                        id="title"
                        name="title"
                        type="text"
                        required
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="description">
                        Description
                    </Label>
                    <Input
                        id="description"
                        name="description"
                        type="textarea"
                        required
                        onChange={(e) => setDescription(e.target.value)}

                    />
                </FormGroup>
                <FormGroup>
                    <Label for="media">
                        Media
                    </Label>
                    <Input
                        id="media"
                        name="media"
                        type="text"
                        required
                        onChange={(e) => setMedia(e.target.value)}

                    />
                </FormGroup>
                <FormGroup>
                    <Label for="media">
                        Copies
                    </Label>
                    <Input
                        id="copies"
                        name="copies"
                        type="number"
                        required
                        onChange={(e) => setCopies(parseInt(e.target.value))}
                    />
                </FormGroup>
                <div className="buttons">
                    <Button className="submit" >
                        Submit
                    </Button>
                    <Button onClick={() => setIsNew(false)} className="btn btn-danger">
                        Cancel
                    </Button>
                </div>
            </Form>
        </Row >
    )
}

export default MintForm;